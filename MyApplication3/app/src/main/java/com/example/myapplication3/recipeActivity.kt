package com.example.myapplication3

import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.Html
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import java.net.URL
import java.security.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.CompletableFuture.runAsync

class recipeActivity : AppCompatActivity() {
    companion object{
        const val description = "description"
        const val instructions = "instructions"
        const val difficulty = "difficulty"
        const val recipeName = "recipeName"
        const val lastUpdated = "lastUpdated"

        const val images = "images"
    }
    private lateinit var recipeNameLable: TextView
    private lateinit var recipeDescriptionLable: TextView
    private lateinit var recipeInstructionsLable: TextView
    private lateinit var recipeDifficultyLable: TextView
    private lateinit var recipelastUpdatedLable: TextView
    private lateinit var recipeImagesNameLayout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)
        recipeNameLable = findViewById(R.id.recipeNameLable)
        recipeDescriptionLable = findViewById(R.id.recipeDescriptionLable)
        recipeInstructionsLable = findViewById(R.id.recipeinstructionsLable)
        recipeDifficultyLable = findViewById(R.id.recipedifficultyLable)
        recipeImagesNameLayout = findViewById(R.id.recipeImagesNameLayout)
        recipelastUpdatedLable = findViewById(R.id.recipelastUpdatedLable)


        val name = intent.getStringExtra(recipeName)
        val instructions = intent.getStringExtra(instructions)
        val difficulty = intent.getStringExtra(difficulty)
        val description = intent.getStringExtra(description)
        val lastUpdated = intent.getStringExtra(lastUpdated)
        val imagelist = intent.getStringArrayExtra((images))
        if (imagelist != null) {
            for (img in imagelist){
                val imageView = ImageView(this)
                imageView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                imageView.setPadding(20,20,20,20)

                Picasso.with(this)
                        .load(img)
                        .placeholder(R.drawable.user_placeholder)
                        .error(R.drawable.user_placeholder_error)
                        .into(imageView);

                recipeImagesNameLayout.addView(imageView);
            }

        }


        recipeNameLable.text = name
        recipeDescriptionLable.text  = description
        recipeDifficultyLable.text  =  difficulty


        val netDate = Date(lastUpdated!!.toLong())


        recipelastUpdatedLable.text  = netDate.toString()

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            recipeInstructionsLable.setText(Html.fromHtml(instructions,Html.FROM_HTML_MODE_LEGACY));
        } else {
            recipeInstructionsLable.setText(Html.fromHtml(instructions));
        }

   }

}
