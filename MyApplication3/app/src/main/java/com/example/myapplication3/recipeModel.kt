package com.example.myapplication3

class recipeModel {
    lateinit var Uuid:String
    lateinit var name:String
    lateinit var images:Array<String>
    lateinit var lastUpdated:String
    var description:String = "no description"
    lateinit var instructions:String
    lateinit var difficulty:String

    constructor(Uuid:String, name:String,  images:Array<String> ,lastUpdated:String, description:String, instructions:String, difficulty:String){
        this.Uuid = Uuid
        this.name = name
        this.images = images
        this.lastUpdated = lastUpdated
        this.description = description
        this.instructions = instructions
        this.difficulty = difficulty
    }

    constructor()

}