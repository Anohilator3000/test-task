package com.example.myapplication3

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View


import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

import android.view.ViewGroup
import android.widget.*


class MainActivity : AppCompatActivity() {

    var arrayList_details:ArrayList<recipeModel> = ArrayList();
    val client = OkHttpClient()

    private lateinit var TestView: TextView
    private lateinit var recipesLayout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recipesLayout = findViewById(R.id.recipesLayout)
        run("https://test.kode-t.ru/recipes.json")
    }

    fun countMe (view: View) {
        TestView.text = arrayList_details.toString()
    }

    fun run(url: String) {
        val request = Request.Builder()
                .url(url)
                .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
            }

            override fun onResponse(call: Call, response: Response) {
                var str_response = response.body()!!.string()
                val json_contact:JSONObject = JSONObject(str_response)
                var jsonarray_info:JSONArray= json_contact.getJSONArray("recipes")
                var size:Int = jsonarray_info.length()
                arrayList_details= ArrayList();
                for (i in 0 until size) {
                    var json_objectdetail:JSONObject=jsonarray_info.getJSONObject(i)
                    val model:recipeModel= recipeModel();
                    var jsonArray:JSONArray = json_objectdetail.getJSONArray("images")

                    val imageArray = Array(jsonArray.length()) {
                        jsonArray.getString(it)
                    }
                    model.images =  imageArray

                    if (json_objectdetail.has("name")) {
                    model.name=json_objectdetail.getString("name")
                    }
                    if (json_objectdetail.has("description")) {
                        model.description=json_objectdetail.getString("description")
                    }
                    if (json_objectdetail.has("instructions")) {
                        model.instructions=json_objectdetail.getString("instructions")
                    }
                    if (json_objectdetail.has("difficulty")) {
                        model.difficulty=json_objectdetail.getString("difficulty")
                    }
                    if (json_objectdetail.has("lastUpdated")) {
                        model.lastUpdated=json_objectdetail.getString("lastUpdated")
                    }

                    arrayList_details.add(model)
                }

                this@MainActivity.runOnUiThread(java.lang.Runnable {

                    for(rec in arrayList_details){
                        val button = Button(this@MainActivity)
                        button.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT)
                        button.text = rec.name
                        button.setOnClickListener(View.OnClickListener {

                        val randomIntent = Intent(this@MainActivity, recipeActivity::class.java)
                        randomIntent.putExtra(recipeActivity.recipeName,rec.name)
                        randomIntent.putExtra(recipeActivity.description,rec.description)
                        randomIntent.putExtra(recipeActivity.instructions,rec.instructions)
                        randomIntent.putExtra(recipeActivity.difficulty,rec.difficulty)
                        randomIntent.putExtra(recipeActivity.lastUpdated,rec.lastUpdated)
                        randomIntent.putExtra(recipeActivity.images,rec.images)
                        startActivity(randomIntent)

                        })
                        button.setPadding(20,20,20,20)
                        recipesLayout.addView(button);
                    }

                })

            }
        })
    }

}
